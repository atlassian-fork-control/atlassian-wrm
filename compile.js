var cheerio = require('cheerio');
var _ = require('./support/underscore');
var fs = require('fs');
var fse = require('fs-extra');
var fsPath = require('path');
var support = require('./support');
var xmlBuilder = require('xmlbuilder');
var moduleParsers = require('./moduleParsers');
var p = support.p;

function configure() {
    return {
        version: 2
    }
}

// Analyses AMD modules defined in `atlassian-plugin.xml` and stores the metadata in `'atlassian-modules.xml'.
//
// - atlassianPluginXmlPath - path to `atlassian-plugin.xml`
//
// Example:
//
//     compile('./atlassian-plugin.xml', function(){console.log('success!')})
//
exports.compile = function compile (atlassianPluginXmlPath, outputDir, cb) {
    var config = configure();
    var absoluteAtlassianPluginXmlPath = fsPath.resolve(atlassianPluginXmlPath);
    var absoluteOutputDir = fsPath.resolve(outputDir);
    var absoluteResourceBaseDir = fsPath.dirname(absoluteAtlassianPluginXmlPath);
    parsePluginXml(absoluteAtlassianPluginXmlPath, absoluteResourceBaseDir, _.fork(cb, function (webModules) {
        findAndParseModules(absoluteResourceBaseDir, webModules, config, _.fork(cb, function (modules) {
            // var json = JSON.stringify(modules, null, 2);
            fse.outputFile(fsPath.join(absoluteOutputDir, 'atlassian-modules.xml'), toXml(modules, config), cb);
        }));
    }));
};

// Parses `atlassian-plugin.xml`.
function parsePluginXml (absoluteAtlassianPluginXmlPath, absoluteResourceBaseDir, cb) {
    fs.readFile(absoluteAtlassianPluginXmlPath, _.fork(function(err) {
        cb(new Error("can't read '" + absoluteAtlassianPluginXmlPath + "'"));
    }, function (xmlData) {
        var $ = cheerio.load(xmlData, {xmlMode: true});

        // Notifying about common mistakes.
        if ($('web-module').length > 0) {
            console.warn("there is the <web-module> declaration found" +
                    ", it's probably a typo, you may want to use <web-modules> instead.");
        }

        // Parsing web modules declarations.
        var $webModulesEls = $('web-modules');
        var webModules = [];
        for (var i = 0; i < $webModulesEls.length; i++)
        {
            var $webModuleEl = $($webModulesEls[i]);

            var webModule = {
                contexts: {}
            };
            webModules.push(webModule);

            var modulesDir = webModule.modulesDir = $webModuleEl.attr('dir');
            if (!modulesDir) {
                return cb(new Error("modules dir not defined!"));
            }
            if (/^\//.test(modulesDir)) {
                return cb(new Error("modules dir should be relative but it's \"" + modulesDir + "\"!"));
            }
            var absoluteModulesDir = fsPath.join(absoluteResourceBaseDir, modulesDir);
            if (_(webModule.modulesDir).isEmpty()) return cb(new Error("attribute `dir` not defined for modules!"));

            // Parsing `inject` element.
            var $injectEls = $webModuleEl.find('inject');
            for (var j = 0; j < $injectEls.length; j++) {
                var $injectEl = $($injectEls[j]);

                // Parsing `module` elements.
                var moduleNames = [];
                var $moduleEls = $injectEl.find('module');
                for (var k = 0; k < $moduleEls.length; k++) {
                    var $moduleEl = $($moduleEls[k]);
                    try {
                        var moduleName = support.normalisePath(absoluteModulesDir, '.', $moduleEl.text());
                    } catch (err) {
                        return cb(err)
                    }
                    if (_(moduleName).isEmpty()) return cb(new Error("no module name for `inject` element!"));
                    moduleNames.push(moduleName);
                }

                // Parsing `context` elements.
                var contextNames = [];
                var $contextEls = $injectEl.find('inject context');
                for (k = 0; k < $contextEls.length; k++) {
                    var $contextEl = $($contextEls[k]);
                    var contextName = $contextEl.text();
                    if (_(contextName).isEmpty()) return cb(new Error("no context name for `inject` element!"));
                    contextNames.push(contextName);
                }

                moduleNames.forEach(function(moduleName) {
                   contextNames.forEach(function(contextName) {
                       if (!webModule.contexts.hasOwnProperty(moduleName)) webModule.contexts[moduleName] = [];
                       webModule.contexts[moduleName].push(contextName);
                   })
                });
            }
        }
        cb(null, webModules);
    }))
}

// Find and parse modules in given path with the given web modules configuration.
function findAndParseModules (absoluteResourceBaseDir, webModules, config, cb) {
    var uniqueModules = {};
    var allModules = [];
    (function next (i) {
        if (i >= webModules.length) return cb(null, allModules);
        var webModule = webModules[i];

        findAndParseModulesIn(absoluteResourceBaseDir, webModule.modulesDir, config, _.fork(cb, function (modules) {
            for (var j = 0; j < modules.length; j++) {
                var module = modules[j];

                // Checking for being unique.
                var uniqueName = module.name + "." + module.type;
                if (allModules.hasOwnProperty(uniqueName)) {
                    return cb(new Error("module `" + name + "` already exist!"));
                }
                uniqueModules[uniqueName] = module;


                module.modulesDescriptorDir = webModule.modulesDir;
                var contexts = webModule.contexts[module.name] || webModule.contexts[module.baseName] || [];
                if (contexts.length > 0) {
                  module.contexts = contexts
                }

                allModules.push(module);
            }

            next(i + 1)
        }));
    })(0);
}

// Find and parse modules in given path.
function findAndParseModulesIn (absoluteResourcesBasePath, modulesDir, config, cb) {
    support.listFiles(fsPath.join(absoluteResourcesBasePath, modulesDir), _.fork(cb, function (filePaths) {
        var modules = [];
        (function next (i) {
            if (i >= filePaths.length) {
                return cb(null, modules);
            }

            var filePath = filePaths[i];
            for (type in moduleParsers) {
                var moduleParser = moduleParsers[type];
                // Checking if it's a module.
                if (moduleParser.isModulePath(filePath)) {
                    // Parsing module.
                    moduleParser.parse(absoluteResourcesBasePath, modulesDir, filePath, _.fork(cb, function (module) {
                        if (module) {
                            modules.push(module);
                        }
                        next(i + 1);
                    }));
                    return;
                }
            }
            next (i + 1);
        })(0);
    }));
}

// Convert modules to XML.
function toXml(modules, config) {
    var modulesEl = xmlBuilder.create('modules');
    modulesEl.attribute('version', config.version);
    for (var i = 0; i < modules.length; i++) {
        var module = modules[i];
        var attrs = {
            "modules-descriptor-dir": module.modulesDescriptorDir,
            type: module.type,
            "src-type": module.srcType,
            name: module.name,
            'base-name': module.baseName,
            "file-path": module.filePath
        };
        if (module.soyNamespace) {
            attrs['soy-namespace'] = module.soyNamespace;
        }
        var moduleEl = modulesEl.ele('module', attrs);
        var dependencies = module.dependencies || {};
        for (var name in dependencies) {
            if (dependencies.hasOwnProperty(name)) {
                var dependency = dependencies[name];
                var dependencyAttrs = {
                    'resolved-name': dependency.resolvedName
                };
                if (dependency.loader) {
                    dependencyAttrs.loader = dependency.loader;
                }
                moduleEl.ele('dependency', dependencyAttrs, name)
            }
        }
        var contexts = module.contexts || [];
        for (var j = 0; j < contexts.length; j++) {
            moduleEl.ele('context', {}, contexts[j])
        }

        var soyTemplates = module.soyTemplates || [];
        for (var k = 0; k < soyTemplates.length; k++) {
            moduleEl.ele('soy-template', {}, soyTemplates[k])
        }

        var soyDependencies = module.soyDependencies || [];
        for (var k = 0; k < soyDependencies.length; k++) {
            moduleEl.ele('soy-dependency', {}, soyDependencies[k])
        }
    }
    var comment = "<!-- This file is autogenerated by atlassian-wrm do not edit it manually, "
            + "see https://bitbucket.org/atlassian/atlassian-wrm for details. -->\n";
    return comment + modulesEl.end({pretty: true}).replace("<?xml version=\"1.0\"?>\n", '');
}
Enables AMD modules in Atlassian Plugin.

How it works - it needed to be run during Atlassian Plugin compilation. It analyses AMD modules in the Plugin and stores 
this analysis in `target/classes/atlassian-modules.xml`. At runtime Web Resource Manager uses this metadata to serve AMD 
modules to the Browser.

# Command-line usage

Install it as global NPM module, you need to have Node.JS and NPM.

``` sh
npm install -g atlassian-wrm
```

Go to your plugin and compile AMD modules

``` sh
cd <your-atlassian-plugin>
wrm-compile
```

# Usage with node.js build tools

``` JavaScript
var compile = require('atlassian-wrm').compile;

compile(atlassianPluginXmlPath, outputDir, function (err) {
    if (err) {
        return console.error(err);
    }
    console.info("AMD modules for \"" + atlassianPluginXmlPath + "\" successfully compiled.");
});
```

# Please provide feedback

Currently it's unclear how we should integrate this tool in Front-End workflow to optimise development experience.

- Hardcode it in AMPS and run it as a step during maven build?
- Distribute it as a minimal npm library and allow Front-End devs to integrate it into their own custom build workflow, in
tools like gulp or grunt?
- How we should handle ES6? Should it be hardcoded and supported out of the box or we should give front-end devs
freedom to choose their own build tools and languages like ES6, TypeScript or CoffeeScript and aim for this 
library being easily integrated in those workflow?
- What other use cases do we need to support?

Also, feel free to hack this script and submit PRs.

# TODO

- It uses a little unusual approach to handle async stuff, maybe it would be better to rewrite it with some classical 
  async helper like https://github.com/kriskowal/q but, it could be done later.
- Allow to specify prefix for AMD modules to search in src or target folders.
- Adapters for grunt, gulp etc.